**1. Установка Create React App:**

npx create-react-app app-name


**2. Переход в папку проекта:**

cd app-name


**3. Установка необходимых пакетов:**

1) npm install --save react-router-dom

2) npm install history qhistory qs

3) npm install redux react-redux redux-thunk


**4. Подключение redux:**

1) В папку src нужно закинуть файл store.js и папку reducers

2) В index.js подключить

```
import { Provider } from 'react-redux';
import store from './store';
```

и заменить ReactDOM.render на

```
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
```

3) Во всех компонентах, которые будут взаимодействовать со store, нужно подключить 

`import { connect } from 'react-redux';`

и изменить export на

`export default connect(state => state)(ComponentName);`


**5. Изменение файловой структуры папки src:**

1) Нужно добавить папку components и перенести туда App.js (при этом не забыть обновить импорт)

2) Для файлов стилей создать папку css и сложить их туда

3) Для изображений создать папку img

4) Для тестов создать папку \_\_tests\_\_ и перенести туда App.test.js
