import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import exampleStoreConfig from './reducers/example';

const createReducer = (initialState, handlers) => {
    return (state = initialState, action) => {
        return (handlers[action.type] && handlers[action.type](state, action)) || state;
    };
};

const exampleReducers = createReducer(exampleStoreConfig.initialState, exampleStoreConfig.actions);

const rootReducer = combineReducers({
    example: exampleReducers,
});

export default createStore(rootReducer, {}, applyMiddleware(thunk));